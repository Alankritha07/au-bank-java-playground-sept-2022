package new_package;

import java.util.Scanner;

public class MortgageCalculator {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("---------Welcome to AU Bank----------");

		Scanner sc = new Scanner(System.in);
		int percent =100;
		int monthsInAYear=12;
		
		float amount =0.0F;
		while(true)
		{
		System.out.println("Enter Loan Amount:");
		amount =sc.nextFloat();
		if(amount <1000 || amount >10000000)
		{
			System.err.println("Please enter the loan amount between 1000 to 10000000");
		}
		else
		{
			break;
		}
		}
		
		float Annual_interest_rate = 0.0F;	
		while(true)
		{
		System.out.println("Enter Annual Interest Rate:");
		Annual_interest_rate =sc.nextFloat();
		
		if(Annual_interest_rate <1 || Annual_interest_rate >30)
		{
			System.err.println("Please enter the interest between 1 to 30");
		}
		else
			break;
		}
		
		float period=0.0F;
		while(true)
		{
		System.out.println("Enter Period in years:");
		period =sc.nextFloat();
		if(period<1 || period>20)
		{
			System.err.println("Please enter the period between 1 to 20");
		}
		else
			break;
		}
		
		
		
		float Monthly_Interest = Annual_interest_rate/percent/monthsInAYear;
		float total_no_of_payments = period*monthsInAYear;
		
		float mortgage=0.0F;
		mortgage = (float) (amount*(Monthly_Interest*Math.pow(1+Monthly_Interest, total_no_of_payments))/(Math.pow(1+Monthly_Interest, total_no_of_payments)-1));
		
		System.out.println("The mortgage amount is "+mortgage);
	
	}
	}




