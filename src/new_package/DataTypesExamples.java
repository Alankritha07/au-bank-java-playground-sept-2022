package new_package;

public class DataTypesExamples {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int numberOfStudents=32;
		System.out.println(numberOfStudents);
		
		long totalKilobytesUsed=30072573265L;
		System.out.println(totalKilobytesUsed);
		
		float temperature = 31.45F;
		System.out.println(temperature);
		
		boolean isElgible=true;
		System.out.println(isElgible);

	}

}
