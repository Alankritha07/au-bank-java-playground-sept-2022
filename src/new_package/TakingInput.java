package new_package;
import java.util.*;

public class TakingInput {

	public static void main(String[] args) {
		double cube=Math.pow(4, 3);
		System.out.println(cube);
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter Name:");
		String name =sc.nextLine();
		System.out.println("Enter Age:");
		int age =sc.nextInt();
		System.out.println("Enter City:");
		String city =sc.nextLine();
		System.out.println("Temperature Today:");
		float temp =sc.nextFloat();
		
		System.out.println("Hey "+name+" you are "+age+" years old");
		System.out.println("Currently you are in "+city);
		System.out.println("Temperature there is "+temp+" degree celsius");
	}

}
