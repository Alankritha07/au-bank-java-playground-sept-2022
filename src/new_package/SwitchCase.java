package new_package;

public class SwitchCase {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String role = "Developer";
		
		switch(role)
		{
		case "Developer":
			System.out.println("Password is Dev@123");
			break;
			
		case "Admin":
			System.out.println("Password is Admin@456");
			break;
			
		case "Tester":
			System.out.println("Password is Tester@123");
			break;
			
		default:
			System.out.println("Password is Guest@123");
			break;
			
		}

	}

}
