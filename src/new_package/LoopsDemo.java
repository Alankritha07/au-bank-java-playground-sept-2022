package new_package;

public class LoopsDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//while loop 
		
		int i=1;
		
		while(i<=10) {
			System.out.println(i);
			i++;
		}

	// do while loop
		
		int j=20;
		
		do {
			System.out.println(j);
			j++;
		}
		while(j<=15);
		
		// for loop 
		
		for(int k=1;k<=10;k++)
		{
			System.out.println(k);
		}
	}

}
