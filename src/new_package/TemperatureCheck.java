package new_package;

public class TemperatureCheck {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		float temperature = 25.5F;
		
		if(temperature > 20 && temperature <30)
		{
			System.out.println("What a beautiful day");
		}
		
		else if (temperature < 20)
		{
			System.out.println("It's cold, wear warm clothes");
		}
		
		else if (temperature > 30)
		{
			System.out.println("It's hot, drink plenty of water");
		}
	}

}
